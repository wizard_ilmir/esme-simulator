/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator;

import java.io.FileNotFoundException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import ru.enterix.esmesimulator.ui.EsmeClient;

/**
 *
 * @author SoftProfi
 */
public class Starter
{

    private static final Logger LOG = Logger.getLogger(Starter.class);
    private static final String log4jPath = "log4j.xml";
    public static void main (String[] args) throws FileNotFoundException
    {
        
        PropertyConfigurator.configure(ClassLoader.getSystemResourceAsStream(log4jPath));
        java.awt.EventQueue.invokeLater(new EsmeClient());
        LOG.info("<p align=\"left\"> Started UI</p>");
       

    }

}
