package ru.enterix.esmesimulator.swingappender;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Layout;
import org.apache.log4j.spi.LoggingEvent;

public class AppenderForUI
        extends AppenderSkeleton {

    private DocumentAppender doc;
    private boolean working = true;

    public AppenderForUI() {
        doc = DocumentAppender.getInstance();
        doc.setAppender(this);
    }

    protected void append(LoggingEvent event) {
        if (!performChecks()) {
            return;
        }
        if (!working) {
            return;
        }
        String logOutput = this.layout.format(event);
        doc.doLog(logOutput);
        if (layout.ignoresThrowable()) {
            String[] lines = event.getThrowableStrRep();
            if (lines != null) {
                int len = lines.length;
                for (int i = 0; i < len; i++) {
                    doc.doLog(lines[i]);
                    doc.doLog(Layout.LINE_SEP);
                }
            }
        }
    }

    public void close() {
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public boolean requiresLayout() {
        return true;
    }

    private boolean performChecks() {
        return !closed && layout != null;
    }

}
