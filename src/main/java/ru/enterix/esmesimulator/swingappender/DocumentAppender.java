/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.swingappender;

import java.io.IOException;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.parser.ParserDelegator;
import org.apache.log4j.AppenderSkeleton;

/**
 *
 * @author ILMIR
 */
public class DocumentAppender extends HTMLDocument {

    private static DocumentAppender doc;
    
    private AppenderSkeleton appender;

    private DocumentAppender() {
        setParser(new ParserDelegator());

    }

    public static synchronized DocumentAppender getInstance() {
        if (doc == null) {
            doc = new DocumentAppender();
        }
        return doc;
    }

    public void doLog(String log) {
        Element body = getElement(doc.getDefaultRootElement(), StyleConstants.NameAttribute, HTML.Tag.BODY);
        try {
            insertAfterStart(body, log);
        } catch (BadLocationException | IOException ex) {
            System.err.println(ex);
        }
    }

    public AppenderSkeleton getAppender() {
        return appender;
    }

    public void setAppender(AppenderSkeleton appender) {
        this.appender = appender;
    }
    
}
