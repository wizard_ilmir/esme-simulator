/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.swingappender;

import java.util.Set;
import java.util.TreeSet;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

/**
 *
 * @author ILMIR
 */
public class FilterPdu extends Filter {

    private Set<String> pdu = new TreeSet<String>();
    
    @Override
    public int decide(LoggingEvent event) {
        String message = (String) event.getMessage();
        boolean filtering = false;
        for (String string : pdu) {
            if (message.indexOf(string) != -1) {
                filtering = true;
                break;
            }
        }

        return filtering ? DENY : ACCEPT;
    }

    public void addFilterPdu(String name) {
        pdu.add(name);
    }

    public void removeFilterPdu(String name) {
      pdu.remove(name);
    }

}
