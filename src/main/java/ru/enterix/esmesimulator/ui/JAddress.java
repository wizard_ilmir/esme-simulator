/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author ILMIR
 */
public class JAddress extends JPanel {

    private JTextField ton;
    private JTextField npi;
    private JTextField address;
    private JLabel labelTon;
    private JLabel labelNpi;
    private JLabel labelAddress;

    public JAddress() {
        initComponents();
    }

    private void initComponents() {
        ton = new JTextField();
        npi = new JTextField();
        address = new JTextField();

        ton.setPreferredSize(new Dimension(50, 25));
        npi.setPreferredSize(new Dimension(50, 25));
        address.setPreferredSize(new Dimension(150, 25));

        labelTon = new JLabel("(ton)");
        labelNpi = new JLabel("(npi)");
        labelAddress = new JLabel("(address)");

        labelTon.setLabelFor(ton);
        labelNpi.setLabelFor(npi);
        labelAddress.setLabelFor(address);

        setLayout(new MigLayout());

        setPreferredSize(new Dimension(300, 27));
        add(labelTon);
        add(ton);
        add(labelNpi);
        add(npi);
        add(labelAddress);
        add(address);

    }

    public String getText() {
        StringBuilder result = new StringBuilder();
        result.append(ton.getText()).append(";");
        result.append(npi.getText()).append(";");
        result.append(address.getText()).append(";");
        return result.toString();
    }

}
