package ru.enterix.esmesimulator.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import org.apache.log4j.Logger;
import ru.enterix.esmesimulator.swingappender.AppenderForUI;
import ru.enterix.esmesimulator.swingappender.DocumentAppender;
import ru.enterix.esmesimulator.swingappender.FilterPdu;

public class LoggerConsole
        extends JPanel {

    private static final Logger LOG = Logger.getLogger(LoggerConsole.class);

    public LoggerConsole() {
        initComponents();
        subscribeToEvents();
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        initButtonsPanel();
        initMessageDispArea();

        this.add(BorderLayout.CENTER, scrollPane);
        this.add(BorderLayout.PAGE_START, buttonsPanel);
    }

    private void initButtonsPanel() {
        enquireLinkCheckBox = new JCheckBox("EnquireLink");

        statusWorkButton = new JButton(PAUSE);
        buttonsPanel = new JPanel();

        clearButton = new JButton("Clear");

        searchField = new JTextField();
        searchField.setPreferredSize(new Dimension(300, 25));
        searchButton = new JButton("Search");
        searchButton.addActionListener(new SearchActionListener());

        buttonsPanel.add(statusWorkButton);
        buttonsPanel.add(clearButton);
        buttonsPanel.add(searchField);
        buttonsPanel.add(searchButton);
        buttonsPanel.add(enquireLinkCheckBox);
    }

    private void initMessageDispArea() {

        logMessagesDisp = new JTextPane();
        logMessagesDisp.setContentType("text/html");
        scrollPane = new JScrollPane(logMessagesDisp);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

    }

    private void eventStatusWorkChange(ActionEvent evt) {
        JButton srcButton = (JButton) evt.getSource();
        AppenderForUI appender = (AppenderForUI) document.getAppender();
        if (srcButton.getText().equals(START)) {
            appender.setWorking(true);
            srcButton.setText(PAUSE);
        } else if (srcButton.getText().equals(PAUSE)) {
            appender.setWorking(false);
            srcButton.setText(START);
        }
    }

    private void eventClearButtonClick() {
        logMessagesDisp.setText("");
    }

    public void setDocument(DocumentAppender doc) {
        logMessagesDisp.setDocument(doc);
        document = doc;
        filterdPdu = (FilterPdu) document.getAppender().getFilter();
        filterdPdu.addFilterPdu("EnquireLink");
    }

    private JButton clearButton; //button to clear the text area
    private JButton searchButton; //search button
    private JTextField searchField; //search field
    private JPanel buttonsPanel; //panel to hold all buttons
    private JTextPane logMessagesDisp; //display area
    private JScrollPane scrollPane;
    private JButton statusWorkButton;
    private JCheckBox enquireLinkCheckBox;
    private DocumentAppender document;
    private FilterPdu filterdPdu;
    public static final String START = "Start";
    public static final String PAUSE = "Pause";

    private void subscribeToEvents() {
        statusWorkButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventStatusWorkChange(ae);
            }
        });
        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventClearButtonClick();
            }
        });

        enquireLinkCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventSelectedenquireLinkCheckBox(ae);
            }
        });
    }

    private void eventSelectedenquireLinkCheckBox(ActionEvent ae) {
        if (!enquireLinkCheckBox.isSelected()) {
            filterdPdu.addFilterPdu("EnquireLink");
        } else {
            filterdPdu.removeFilterPdu("EnquireLink");
        }
    }

    class SearchActionListener
            implements ActionListener {

        public void actionPerformed(ActionEvent evt) {
            JButton srcButton = (JButton) evt.getSource();
            if (!"Search".equals(srcButton.getText())) {
                return;
            }
            String searchTerm = searchField.getText();
            if (searchTerm == null || searchTerm.equals("")) {
                return;
            }
            try {
                String allLogText = document.getText(0, document.getLength());

                int startIndex = 0;
                int selectionIndex = -1;
                Highlighter hLighter = logMessagesDisp.getHighlighter();
                hLighter.removeAllHighlights();
                DefaultHighlighter.DefaultHighlightPainter highlightPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.BLUE);
                while ((selectionIndex = allLogText.indexOf(searchTerm, startIndex)) != -1) {
                    startIndex = selectionIndex + searchTerm.length();
                    try {
                        hLighter.addHighlight(selectionIndex, (selectionIndex + searchTerm.length()),
                                highlightPainter);

                    } catch (BadLocationException ble) {
                        System.out.println("Bad Location Exception: " + ble.getMessage());
                    }
                }
            } catch (BadLocationException ex) {
                throw new RuntimeException(ex);
            }
        }

        private int getNumberOfNewLinesTillSelectionIndex(String allLogText, int selectionIndex) {
            int numberOfNewlines = 0;
            int pos = 0;
            while ((pos = allLogText.indexOf("\n", pos)) != -1 && pos <= selectionIndex) {
                numberOfNewlines++;
                pos++;
            }
            return numberOfNewlines;
        }
    }
}
