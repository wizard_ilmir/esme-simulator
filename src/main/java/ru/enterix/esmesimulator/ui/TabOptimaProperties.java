/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui;

import java.awt.BorderLayout;
import java.lang.reflect.Field;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import net.miginfocom.swing.MigLayout;
import static ru.enterix.esmesimulator.ui.EsmeClient.SPEED_SCROLL;

/**
 *
 * @author SoftProfi
 */
public class TabOptimaProperties
extends JPanel
{

    public TabOptimaProperties ()
    {
        initComponents();
    }

    
    private void initComponents ()
    {
        centerPanel= new JPanel(new MigLayout());
        scrollForCenterPanel=new JScrollPane();

        this.setLayout(new BorderLayout());
        
        scrollForCenterPanel.getVerticalScrollBar().setUnitIncrement(SPEED_SCROLL);
        scrollForCenterPanel.setViewportView(centerPanel);
        
        this.add(scrollForCenterPanel,BorderLayout.CENTER);
    }
    
      public void setComponentsData (List<Field> properties )
    {
        centerPanel.removeAll();
              
        centerPanel.updateUI();
        centerPanel.validate();
    }

    private JPanel centerPanel;
    private JScrollPane scrollForCenterPanel;
}
