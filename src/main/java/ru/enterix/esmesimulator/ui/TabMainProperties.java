/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.beans.Statement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;
import org.apache.log4j.Logger;
import ru.enterix.esmesimulator.Classes;
import static ru.enterix.esmesimulator.Classes.getSetterName;
import ru.enterix.esmesimulator.Parser;
import static ru.enterix.esmesimulator.Parser.parseByte;
import static ru.enterix.esmesimulator.Parser.parseInt;
import static ru.enterix.esmesimulator.Parser.parseShort;
import ru.enterix.esmesimulator.exceptions.ParserInvalidValueException;
import static ru.enterix.esmesimulator.ui.EsmeClient.SPEED_SCROLL;
import ru.enterix.nettysmpplib.exceptions.InvalidLengthStringException;
import ru.enterix.nettysmpplib.pdus.Address;
import ru.enterix.nettysmpplib.pdus.AddressRange;
import ru.enterix.nettysmpplib.pdus.Pdu;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;

/**
 *
 * @author SoftProfi
 */
public class TabMainProperties
        extends JPanel {

    private static final Logger LOG = Logger.getLogger(TabMainProperties.class);

    private JScrollPane centerScroll;
    private JPanel topPanel;
    private JPanel centerPanel;
    private Pdu pdu;
    private HashMap<Integer, List<JProperties>> cacheJProperties = new HashMap<>();
    private List<JProperties> currentJProperties;
    private static final Pattern regexAddresss = Pattern.compile("^(.*);(.*);(.*);$");

    public TabMainProperties() {
        initComponents();
    }

    public void initComponents() {
        centerScroll = new JScrollPane();
        topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        centerPanel = new JPanel();

        centerPanel.setLayout(new MigLayout());

        centerScroll.getVerticalScrollBar().setUnitIncrement(SPEED_SCROLL);
        centerScroll.setAutoscrolls(true);
        centerScroll.setViewportView(centerPanel);

        this.setLayout(new BorderLayout());
        this.add(centerScroll, BorderLayout.CENTER);
        this.add(topPanel, BorderLayout.NORTH);
    }

    public void setPdu(Pdu pdu) {
        this.pdu = pdu;
        List<Field> field = Classes.getProperties(pdu.getClass());
        currentJProperties = createJProperties(pdu, field);
        setFieldInJProperties(currentJProperties, field);
        showJProperties(currentJProperties);

    }

    public Pdu getPdu() {
        for (JProperties jProperties : currentJProperties) {
            String text = jComponentGetText(jProperties.textField);
            if (text.isEmpty()) {
                continue;
            }
            try {
                jProperties.field.setAccessible(true);
                Object[] paramenrts = setterGetParamerts(text, jProperties.field.getType());
                if (paramenrts == null) {
                    continue;
                }

                Statement st = new Statement(pdu, getSetterName(jProperties.field.getName()), paramenrts);
                st.execute();
            } catch (InvalidLengthStringException | ParserInvalidValueException ex) {
                StringBuilder errorLogBuffer = new StringBuilder();
                errorLogBuffer.append("<span align=\"left\" color=\"red\">");
                errorLogBuffer.append("Error in field=").append(jProperties.label.getText());
                errorLogBuffer.append(": ");
                errorLogBuffer.append(ex.getMessage());
                errorLogBuffer.append("<span>");
                LOG.error(errorLogBuffer.toString());
                return null;
            } catch (Exception ex) {
                LOG.error(ex, ex);
                return null;
            }
        }
        return pdu;
    }

    private Object[] setterGetParamerts(String value, Class<?> typeField) throws ParserInvalidValueException {

        Object[] result = null;
        if (typeField == byte.class) {
            result = new Object[1];
            result[0] = parseByte(value);
        } else if (typeField == short.class) {
            result = new Object[1];
            result[0] = parseShort(value);
        } else if (typeField == int.class) {
            result = new Object[1];
            result[0] = parseInt(value);
        } else if (typeField == String.class) {
            result = new Object[1];
            result[0] = value;
        } else if (typeField == Address.class || typeField == AddressRange.class) {
            if (value.isEmpty()) {
                return null;
            } else {
                Matcher matcherAddress = regexAddresss.matcher(value);
                result = new Object[3];
                if (matcherAddress.matches()) {
                    result[0] = Parser.parseByte(matcherAddress.group(1));
                    result[1] = Parser.parseByte(matcherAddress.group(2));
                    result[2] = matcherAddress.group(3);
                } else {
                    String descriptionError = String.format("Unable to convert the \"%s\" to %s", value, typeField.getName());
                    ParserInvalidValueException newEx = new ParserInvalidValueException(descriptionError);
                    newEx.setInvalidValue(value);
                    newEx.setParseType(typeField);

                    throw newEx;
                }
            }
        }

        return result;
    }

    private void setFieldInJProperties(List<JProperties> jProperties, List<Field> fields) {
        Iterator<JProperties> iteratorProperties = jProperties.iterator();
        Iterator<Field> iteratorField = fields.iterator();

        while (iteratorProperties.hasNext()) {
            iteratorProperties.next().field = iteratorField.next();
        }
    }

    private List<JProperties> createJProperties(Pdu pdu, List<Field> fields) {

        List<JProperties> jPropertiesesFromCache = cacheJProperties.get(pdu.getId());
        if (jPropertiesesFromCache != null) {

            return jPropertiesesFromCache;
        }

        List<JProperties> jPropertieses = new ArrayList<>(fields.size());
        for (Field f : fields) {

            if (TLV.class.isAssignableFrom(f.getType())) {
                break;
            }
            JLabel label = new JLabel(f.getName());
            JComponent tf = getJComponent(f);
            label.setLabelFor(tf);
            jPropertieses.add(new JProperties(label, tf));

        }
        cacheJProperties.put(pdu.getId(), jPropertieses);

        return jPropertieses;
    }

    private void showJProperties(List<JProperties> jPropertieses) {
        centerPanel.removeAll();
        for (JProperties jProperties : jPropertieses) {
            centerPanel.add(jProperties.label);
            centerPanel.add(jProperties.textField, "wrap");
        }
        centerPanel.updateUI();
    }

    private JComponent getJComponent(Field f) {
        JComponent result;
        if (f.getType().equals(Address.class) || f.getType().equals(AddressRange.class)) {
            result = new JAddress();
            result.setPreferredSize(new Dimension(500, 50));
            result.setName(f.getName());
        } else {
            result = new JTextField();
            result.setPreferredSize(new Dimension(200, 25));
            result.setName(f.getName());
        }

        return result;

    }

    private String jComponentGetText(JComponent jc) {
        String result = "";
        if (jc.getClass().equals(JTextField.class)) {
            result = ((JTextField) jc).getText();
        } else if (jc.getClass().equals(JAddress.class)) {
            result = ((JAddress) jc).getText();
        }

        return result;

    }

    class JProperties {

        public JLabel label;
        public JComponent textField;
        public Field field;

        public JProperties(JLabel label, JComponent textField) {
            this.label = label;
            this.textField = textField;

        }

    }

}
