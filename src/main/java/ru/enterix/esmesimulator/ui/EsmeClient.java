/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;
import ru.enterix.esmesimulator.Parser;
import ru.enterix.esmesimulator.data.HelperJaxb;
import ru.enterix.esmesimulator.data.pduDescription.PduDescription;
import ru.enterix.esmesimulator.data.pduDescription.PduDescriptions;
import ru.enterix.esmesimulator.data.profile.Profile;
import ru.enterix.esmesimulator.data.profile.ProfilesModel;
import ru.enterix.esmesimulator.network.ClientSmppHandler;
import ru.enterix.esmesimulator.swingappender.DocumentAppender;
import ru.enterix.esmesimulator.ui.settings.Settings;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public class EsmeClient
        extends javax.swing.JFrame implements Runnable {

    public static final int SPEED_SCROLL = 16;
    private static final Logger LOG = Logger.getLogger(EsmeClient.class);
    private static final String CONNECT = "Connect";
    private static final String DISCONNECT = "Disconnect";
    private ProfilesModel modelForProfile;

    public EsmeClient() {

        initComponents();
        subscribeToEvents();
    }

    private void initComponents() {
        jPanelTop = new JPanel();
        connectjButton = new JButton();
        disconnectjButton = new JButton();
        hostsjComboBox = new JComboBox();
        settingjButton = new JButton();
        jTabbedPaneSenter = new JTabbedPane();
        workspacejPanel = new JPanel();
        pdusjScrollPane = new JScrollPane();
        pdusjList = new JList<>();
        controljPanel = new JPanel();
        submitjButton = new JButton();
        configjParamjTabbedPane = new JTabbedPane();
        tabMandatoryParamUI = new TabMainProperties();
        tabOptimalParamUI = new TabOptimaProperties();
        loggerUI = new LoggerConsole();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("ESME Client");
        setName("Connect");
        setPreferredSize(new Dimension(1000, 700));

        jPanelTop.setPreferredSize(new Dimension(73, 23));
        jPanelTop.setLayout(new BoxLayout(jPanelTop, BoxLayout.LINE_AXIS));

        connectjButton.setText("Connect");
        jPanelTop.add(connectjButton);

        disconnectjButton.setText("Disconnect");
        disconnectjButton.setEnabled(false);
        jPanelTop.add(disconnectjButton);

        hostsjComboBox.setMinimumSize(new Dimension(20, 20));
        modelForProfile = ProfilesModel.getInstance();
        hostsjComboBox.setModel((ComboBoxModel) modelForProfile);
        jPanelTop.add(hostsjComboBox);

        settingjButton.setText("Settings");
        jPanelTop.add(settingjButton);

        getContentPane().add(jPanelTop, BorderLayout.PAGE_START);
        jTabbedPaneSenter.setPreferredSize(new Dimension(400, 300));
        workspacejPanel.setLayout(new BorderLayout());

        pdusjList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        PduDescriptions pduDercriptions = (PduDescriptions) HelperJaxb.load(PduDescriptions.class);

        modelPduList = new DefaultListModel<>();
        for (PduDescription pdu : pduDercriptions.getList()) {
            modelPduList.addElement(pdu);
        }

        pdusjList.setModel(modelPduList);

        pdusjScrollPane.setPreferredSize(new Dimension(300, 130));
        pdusjScrollPane.setViewportView(pdusjList);
        pdusjScrollPane.getVerticalScrollBar().setUnitIncrement(SPEED_SCROLL);

        workspacejPanel.add(pdusjScrollPane, BorderLayout.WEST);

        controljPanel.setBackground(new Color(227, 227, 227));
        controljPanel.setBorder(BorderFactory.createCompoundBorder());
        controljPanel.setPreferredSize(new Dimension(917, 35));
        controljPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        submitjButton.setText("Submit");
        submitjButton.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        submitjButton.setEnabled(false);
        submitjButton.setHorizontalTextPosition(SwingConstants.RIGHT);
        controljPanel.add(submitjButton);
        workspacejPanel.add(controljPanel, BorderLayout.SOUTH);

        configjParamjTabbedPane.setPreferredSize(new Dimension(600, 600));
        configjParamjTabbedPane.addTab("Mandatory parameters", tabMandatoryParamUI);
        configjParamjTabbedPane.addTab("Optimal parameters", tabOptimalParamUI);

        workspacejPanel.add(configjParamjTabbedPane, BorderLayout.CENTER);
        jTabbedPaneSenter.addTab("Main", workspacejPanel);
        getContentPane().add(jTabbedPaneSenter, BorderLayout.CENTER);

        loggerUI.setPreferredSize(new Dimension(500, 300));
        loggerUI.setDocument(DocumentAppender.getInstance());

        getContentPane().add(loggerUI, BorderLayout.PAGE_END);
        getRootPane().setDefaultButton(submitjButton);

        pack();
        setLocationRelativeTo(null);
    }

    private void subscribeToEvents() {
        connectjButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                eventClickConnectionButton(e);
            }
        });
        disconnectjButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                eventClickConnectionButton(e);
            }
        });
        pdusjList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                eventSelectionItemInListPdus(e);
            }
        });
        submitjButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventClickSubmitButton(ae);
            }
        });
        settingjButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventClickSettingsButton(ae);
            }
        });
    }

    private void eventSelectionItemInListPdus(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            PduDescription selectedObj = pdusjList.getSelectedValue();
            int id = Parser.parseInt(selectedObj.getId());
            Pdu pdu = Pdu.createPdu(id);
            tabMandatoryParamUI.setPdu(pdu);

        }
    }

    private void eventClickConnectionButton(ActionEvent evt) {

        if (evt.getActionCommand().equalsIgnoreCase(CONNECT)) {
            try {
                Profile profile = (Profile) hostsjComboBox.getSelectedItem();
                if (profile == null) {
                    LOG.error("Not set host");
                    return;
                }
                clientSmpp.connecting(profile.getHost().toInetSocketAddress());
                clientSmpp.addDisconnectListener(new DisconnectListener() {
                    public void Disconnected() {
                        editStatusConnection(DISCONNECT);
                    }
                });
                editStatusConnection(evt.getActionCommand());
            } catch (Exception ex) {
                LOG.error(ex);
            }
        } else {
            editStatusConnection(evt.getActionCommand());
            clientSmpp.desconecting();
        }
    }

    private void eventClickSubmitButton(ActionEvent evt) {
        Pdu pdu = tabMandatoryParamUI.getPdu();
        if (pdu == null) {
            return;
        }
        clientSmpp.writeToChannel(pdu);

    }

    private void eventClickSettingsButton(ActionEvent evt) {
        Settings settingsDialog = new Settings();
        java.awt.EventQueue.invokeLater(settingsDialog);
    }

    private void editStatusConnection(String status) {
        if (status.equalsIgnoreCase(CONNECT)) {
            connectjButton.setEnabled(false);
            disconnectjButton.setEnabled(true);
            submitjButton.setEnabled(true);
        } else {
            connectjButton.setEnabled(true);
            disconnectjButton.setEnabled(false);
            submitjButton.setEnabled(false);
        }
    }

    private DefaultListModel<PduDescription> modelPduList;
    private JTabbedPane configjParamjTabbedPane;
    private JButton connectjButton;
    private JPanel controljPanel;
    private JButton disconnectjButton;
    private JComboBox hostsjComboBox;
    private JPanel jPanelTop;
    private JTabbedPane jTabbedPaneSenter;
    private LoggerConsole loggerUI;
    private JList<PduDescription> pdusjList;
    private JScrollPane pdusjScrollPane;
    private JButton settingjButton;
    private JButton submitjButton;
    private TabMainProperties tabMandatoryParamUI;
    private TabOptimaProperties tabOptimalParamUI;
    private JPanel workspacejPanel;
    private ClientSmppHandler clientSmpp = new ClientSmppHandler();

    @Override
    public void run() {
        setVisible(true);
    }

}
