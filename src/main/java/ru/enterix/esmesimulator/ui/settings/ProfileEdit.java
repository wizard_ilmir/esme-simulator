/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui.settings;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import org.apache.log4j.Logger;
import ru.enterix.esmesimulator.data.profile.Host;
import ru.enterix.esmesimulator.data.profile.Profile;

/**
 *
 * @author SoftProfi
 */
public class ProfileEdit 
        extends javax.swing.JDialog
        implements Runnable {

    private static final Logger LOG = Logger.getLogger(ProfileEdit.class);
 
    public ProfileEdit() {
        super(new JFrame(), true);
        initComponents();
        subscribeToEvents();
        observersEventSave = new LinkedList<>();
    }

    private void initComponents() {
        mainContolPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        saveJButton = new JButton("Save");
        closeJButton = new JButton("Close");

        profileEditMainInformation = new ProfileEditMainInformation();
        profileEditJTabbedPane = new JTabbedPane();

        profileEditJTabbedPane.addTab("Main profile information", profileEditMainInformation);
      
        mainContolPanel.add(saveJButton);
        mainContolPanel.add(closeJButton);

        this.setLayout(new BorderLayout());
        this.add(profileEditJTabbedPane, BorderLayout.CENTER);
        this.add(mainContolPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Profile");
        setResizable(false);
        setPreferredSize(new Dimension(500, 300));

        pack();
        setLocationRelativeTo(null);
    }

    public void setProfile(Profile profile) {
        profileEditMainInformation.setNameProfile(profile.getName());
        profileEditMainInformation.setAddress(profile.getHost().getAddress());
        profileEditMainInformation.setPort(profile.getHost().getPort());

    }

    public void addSaveListener(SaveProfileListener l) {
        observersEventSave.add(l);
    }

    @Override
    public void run() {
        setVisible(true);
    }

    private void eventSaveProfile(ActionEvent e) {
        if (!profileEditMainInformation.isValidation()) {
            LOG.error("Не все поля заполнены в редактированном/новом профиле");
            return;
        }

        Profile profile = new Profile();
        profile.setName(profileEditMainInformation.getNameProfile());
        Host host = new Host();
        host.setAddress(profileEditMainInformation.getAddress());
        host.setPort(profileEditMainInformation.getPort());
        profile.setHost(host);
        for (SaveProfileListener saveProfileListener : observersEventSave) {
            saveProfileListener.saved(profile);
        }
        setVisible(false);

    }

    public void eventCloseButtonClick() {
        setVisible(false);
    }

    private ProfileEditMainInformation profileEditMainInformation;
    private JTabbedPane profileEditJTabbedPane;
    private JPanel mainContolPanel;
    private JButton saveJButton;
    private JButton closeJButton;
    private List<SaveProfileListener> observersEventSave;

    private void subscribeToEvents() {
        saveJButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventSaveProfile(ae);
            }
        });
        closeJButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventCloseButtonClick();
            }
        });
    }
}
