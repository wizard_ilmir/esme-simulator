/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.enterix.esmesimulator.ui.settings;

import ru.enterix.esmesimulator.data.profile.Profile;

/**
 *
 * @author ILMIR
 */
public interface SaveProfileListener {
    
    public void saved(Profile profile);
}
