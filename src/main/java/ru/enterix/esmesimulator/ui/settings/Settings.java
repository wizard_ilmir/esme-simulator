/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui.settings;

import ru.enterix.esmesimulator.ui.settings.ProfilesView;
import java.awt.Dimension;
import javax.swing.JDialog;
import ru.enterix.esmesimulator.data.profile.ProfilesModel;

/**
 *
 * @author SoftProfi
 */
public class Settings
extends JDialog implements Runnable
{
    public Settings ()
    {
        super(new javax.swing.JFrame(), true);
        initComponents();
    }
    
    private void initComponents()
    {
        jTabbedPane1 = new javax.swing.JTabbedPane();
        mainjPanel = new javax.swing.JPanel();
        profilesView = new ProfilesView(ProfilesModel.getInstance());
        
        setResizable(false);
        mainjPanel.setLayout(new java.awt.BorderLayout());
        mainjPanel.add(profilesView, java.awt.BorderLayout.CENTER);

        jTabbedPane1.addTab("Profile", mainjPanel);
        getContentPane().add(jTabbedPane1, java.awt.BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(500, 500));
        pack();
        setLocationRelativeTo(null);
    }

    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel mainjPanel;
    private ru.enterix.esmesimulator.ui.settings.ProfilesView profilesView;

    @Override
    public void run() {
        setVisible(true);
    }
}
