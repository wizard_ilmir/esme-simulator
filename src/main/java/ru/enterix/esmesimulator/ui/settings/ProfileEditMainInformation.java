/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui.settings;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author SoftProfi
 */
public class ProfileEditMainInformation
        extends JPanel {

    public static final Dimension TEXT_FIELD_SIZE = new Dimension(250, 25);

    public ProfileEditMainInformation() {
        initComponents();
    }

    private void initComponents() {
        this.setLayout(new MigLayout());

        namejLabel = new JLabel("Name profile:");
        addressjLabel = new JLabel("Host address:");
        portjLabel = new JLabel("Host Port:");

        namejTextField = new JTextField();
        addresstjTextField = new JTextField();
        portjTextField = new JTextField();

        namejLabel.setLabelFor(namejTextField);
        addressjLabel.setLabelFor(addresstjTextField);
        portjLabel.setLabelFor(portjTextField);

        namejTextField.setPreferredSize(TEXT_FIELD_SIZE);
        addresstjTextField.setPreferredSize(TEXT_FIELD_SIZE);
        portjTextField.setPreferredSize(TEXT_FIELD_SIZE);

        this.add(namejLabel);
        this.add(namejTextField, "wrap");
        this.add(addressjLabel);
        this.add(addresstjTextField, "wrap");
        this.add(portjLabel);
        this.add(portjTextField);
    }
    private javax.swing.JLabel addressjLabel;
    private javax.swing.JTextField addresstjTextField;
    private javax.swing.JLabel namejLabel;
    private javax.swing.JTextField namejTextField;
    private javax.swing.JLabel portjLabel;
    private javax.swing.JTextField portjTextField;

    public String getAddress() {
        return addresstjTextField.getText();
    }

    public String getNameProfile() {
        return namejTextField.getText();
    }

    public String getPort() {
        return portjTextField.getText();
    }

    public void setAddress(String host) {
        addresstjTextField.setText(host);
    }

    public void setNameProfile(String name) {
        namejTextField.setText(name);
    }

    public void setPort(String port) {
        portjTextField.setText(port);
    }

    public boolean isValidation() {
        if (portjTextField.getText().isEmpty() || addresstjTextField.getText().isEmpty()
                || namejTextField.getText().isEmpty() || namejTextField.getText().isEmpty()) {
            return false;
        } else {
            return true;
        }

    }
}
