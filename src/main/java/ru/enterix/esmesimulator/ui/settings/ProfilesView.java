/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.ui.settings;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import ru.enterix.esmesimulator.data.profile.Profile;
import ru.enterix.esmesimulator.data.profile.ProfilesModel;

/**
 *
 * @author SoftProfi
 */
public class ProfilesView
        extends JPanel {

    public ProfilesView(ProfilesModel profilesModel) {
        initComponents();
        subscribeToEvents();
        this.profilesModel = profilesModel;
        this.listProfiles.setModel(profilesModel);
    }

    private void eventAddProfile() {
        ProfileEdit profileEdit = new ProfileEdit();

        profileEdit.addSaveListener(new SaveProfileListener() {
            public void saved(Profile profile) {
                profilesModel.addElement(profile);
                profilesModel.save();
            }
        });
        java.awt.EventQueue.invokeLater(profileEdit);
    }

    private void eventEditProfile() {
        Profile selectedProfile = (Profile) listProfiles.getSelectedValue();
        if (selectedProfile == null) {
            return;
        }
        ProfileEdit profileEdit = new ProfileEdit();
        profileEdit.setProfile(selectedProfile);

        profileEdit.addSaveListener(new SaveProfileListener() {
            public void saved(Profile profile) {
                Profile replace = profile;
                Profile find = (Profile) listProfiles.getSelectedValue();
                int index = profilesModel.indexOf(find, 0);
                profilesModel.remove(index);
                profilesModel.insertElementAt(replace, index);
                profilesModel.save();
            }
        });

        java.awt.EventQueue.invokeLater(profileEdit);
    }

    private void eventDeleteProfile() {
        Profile selectedProfile = (Profile) listProfiles.getSelectedValue();
        if (selectedProfile == null) {
            return;
        }
        profilesModel.removeElement(selectedProfile);
        profilesModel.save();
    }

    private void eventClickListProfiles(MouseEvent me) {
        if (me.getClickCount() == 2) {
            eventEditProfile();
        }
    }

    private void initComponents() {
        botControlPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        scrollForListProfiles = new JScrollPane();
        listProfiles = new JList();
        addButton = new JButton("Add");
        editButton = new JButton("Edit");
        deleteButton = new JButton("Delete");

        scrollForListProfiles.setViewportView(listProfiles);

        botControlPanel.add(addButton);
        botControlPanel.add(editButton);
        botControlPanel.add(deleteButton);

        listProfiles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        this.setLayout(new BorderLayout());
        this.add(scrollForListProfiles, BorderLayout.CENTER);
        this.add(botControlPanel, BorderLayout.NORTH);

    }
    private JPanel botControlPanel;
    private JScrollPane scrollForListProfiles;
    private JList listProfiles;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;
    private ProfilesModel profilesModel;

    private void subscribeToEvents() {
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventAddProfile();
            }
        });
        editButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventEditProfile();
            }
        });
        deleteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                eventDeleteProfile();
            }
        });
        listProfiles.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                eventClickListProfiles(me);
            }
        });
    }
}
