/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.exceptions;

/**
 *
 * @author SoftProfi
 */
public class NotConnectToServerException extends Exception

{

    public NotConnectToServerException ()
    {
    }

    public NotConnectToServerException (String message)
    {
        super(message);
    }

    public NotConnectToServerException (Throwable cause)
    {
        super(cause);
    }
    
    
}
