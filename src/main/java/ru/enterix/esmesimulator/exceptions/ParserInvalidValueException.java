/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.exceptions;

/**
 *
 * @author ILMIR
 */
public class ParserInvalidValueException extends RuntimeException {

    private String invalidValue;
    private Class<?> parseType;
    
    public ParserInvalidValueException() {
    }

    public ParserInvalidValueException(String string) {
        super(string);
    }

    public ParserInvalidValueException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public ParserInvalidValueException(Throwable thrwbl) {
        super(thrwbl);
    }

    public String getInvalidValue() {
        return invalidValue;
    }

    public void setInvalidValue(String invalidValue) {
        this.invalidValue = invalidValue;
    }

    public Class<?> getParseType() {
        return parseType;
    }

    public void setParseType(Class<?> parseType) {
        this.parseType = parseType;
    }
    
}
