/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.pduDescription;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author SoftProfi
 */
public class PduDescription
{
    private String name;
    private String id;

    public PduDescription ()
    {
    }

    public PduDescription (String name, String id)
    {
        this.name = name;
        this.id = id;
    }

    @XmlElement(required = true )
    public void setName (String name)
    {
        this.name = name;
    }

    @XmlElement(required = true)
    public void setId (String id)
    {
        this.id = id;
    }
    
    

    public String getName ()
    {
        return name;
    }

    public String getId ()
    {
        return id;
    }

    @Override
    public String toString ()
    {
        return name;
    }
    
}
