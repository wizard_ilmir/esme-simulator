/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.pduDescription;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SoftProfi
 */
@XmlRootElement
public class PduDescriptions 
{

    @XmlElement(name = "pduDescription", required = true)
    private List<PduDescription> pduDescriptions;

    public PduDescriptions ()
    {
    }

  
    public List<PduDescription> getList ()
    {
        if(pduDescriptions==null){
            pduDescriptions=new ArrayList<>();
        }
        return pduDescriptions;
    }


    @XmlTransient
    public void setList (
    List<PduDescription> list)
    {
        pduDescriptions=list;
    }
    
}
