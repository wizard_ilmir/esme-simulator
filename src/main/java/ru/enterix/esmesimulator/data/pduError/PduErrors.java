/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.pduError;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SoftProfi
 */
@XmlRootElement
public class PduErrors
{

    @XmlElement(name = "error", required = true)
    private List<PduError> listError;

    public static boolean isError (PduError value)
    {
        if (value.getName().equalsIgnoreCase("ESME_ROK")) {
            return false;
        } else {
            return true;
        }
    }

    public List<PduError> getList ()
    {
        if(listError==null){
            listError = new ArrayList<>();
        }
        return listError;
    }

    @XmlTransient
    public void setList (
    List<PduError> list)
    {
        listError = list;
    }
    
    

}
