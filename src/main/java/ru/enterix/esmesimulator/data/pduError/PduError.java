/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.pduError;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author SoftProfi
 */
public class PduError
{

    private String name;
    private String value;
    private String description;

    public PduError ()
    {
    }
    

    public PduError (String name, String value, String description)
    {
        this.name = name;
        this.value = value;
        this.description = description;
    }

    @XmlAttribute(required = true)
    public void setName (String name)
    {
        this.name = name;
    }

    @XmlAttribute(required = true)
    public void setValue (String value)
    {
        this.value = value;
    }

    @XmlElement(required = true)
    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getName ()
    {
        return name;
    }

    public String getValue ()
    {
        return value;
    }

    public String getDescription ()
    {
        return description;
    }
    

}
