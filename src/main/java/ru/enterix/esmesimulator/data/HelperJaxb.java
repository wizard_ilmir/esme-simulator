/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.apache.log4j.Logger;

/**
 *
 * @author SoftProfi 
 */
public class HelperJaxb
{

    private static final Logger LOG = Logger.getLogger(HelperJaxb.class);
    private static final String XML_PATH = "mutable-resources/xml/";

    public static Object load (Class<?> clazz, String nameFile)
    {
        InputStream xmlStream = ClassLoader.getSystemResourceAsStream(nameFile);
        Object result = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result = jaxbUnmarshaller.unmarshal(xmlStream);
        } catch (JAXBException ex) {
            LOG.error(ex);
        }
        return result;
    }

    public static Object load (Class<?> clazz)
    {
        
        Object result = null;
        try {
            File fileDataXml= new File("mutable-resources/xml/",clazz.getSimpleName() + ".xml");
            InputStream xmlStream = new FileInputStream(fileDataXml);
            
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            result = jaxbUnmarshaller.unmarshal(xmlStream);
        } catch (JAXBException  | FileNotFoundException ex) {
            LOG.error(ex);
        }
        return result;
    }

    public static void save (Object obj, String nameFile)
    {

        try {
            OutputStream xmlStream = new FileOutputStream(ClassLoader.getSystemResource(nameFile).getPath());

            //ClassLoader.getSystemResourceAsStream(nameFile);
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(obj, xmlStream);
        } catch (JAXBException | FileNotFoundException ex) {
            LOG.error(ex);
        }

    }

    public static void save (Object obj)
    {

        try {
            File fileDataXml= new File("mutable-resources/xml/",obj.getClass().getSimpleName() + ".xml");
            OutputStream xmlStream = new FileOutputStream(fileDataXml);
                
            JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(obj, xmlStream);
        } catch (JAXBException | FileNotFoundException ex) {
            LOG.error(ex);
        }

    }
}
