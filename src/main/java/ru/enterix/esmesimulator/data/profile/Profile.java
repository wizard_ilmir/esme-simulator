/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.profile;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author SoftProfi
 */
public class Profile
{

    private String name;
    private Host host;

    public Profile ()
    {
    }

    public Profile (String name, Host host)
    {
        this.name = name;
        this.host = host;
    }

    @XmlAttribute(required = true)
    public void setName (String name)
    {
        this.name = name;
    }

    @XmlElement(required = true)
    public void setHost (Host host)
    {
        this.host = host;
    }

    public String getName ()
    {
        return name;
    }

    public Host getHost ()
    {
        return host;
    }

    @Override
    public String toString ()
    {
        return name;
    }

    @Override
    public boolean equals (Object obj)
    {
        if (obj == null && !(obj instanceof Profile)) {
            return false;
        }
        Profile profile=(Profile)obj;
        return  profile.name.equals(this.name);
    }
    
}
