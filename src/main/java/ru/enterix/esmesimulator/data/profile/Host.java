/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.profile;

import java.net.InetSocketAddress;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author SoftProfi
 */
public class Host
{

    private String address;
    private String port;

    public Host ()
    {
    }

    public String getAddress ()
    {
        return address;
    }

    public String getPort ()
    {
        return port;
    }

    @XmlAttribute(required = true)
    public void setAddress (String address)
    {
        this.address = address;
    }

    @XmlAttribute(required = true)
    public void setPort (String port)
    {
        this.port = port;
    }

    public InetSocketAddress toInetSocketAddress ()
    {
        InetSocketAddress result = new InetSocketAddress(address, Integer.valueOf(port));
        return result;
    }

    @Override
    public String toString ()
    {
        return address + ":" + port;
    }
    
    

}
