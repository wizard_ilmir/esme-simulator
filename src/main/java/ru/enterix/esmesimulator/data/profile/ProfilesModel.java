/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.profile;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListModel;
import ru.enterix.esmesimulator.data.HelperJaxb;

/**
 *
 * @author SoftProfi
 */
public class ProfilesModel
        extends DefaultListModel<Profile> implements ComboBoxModel<Profile> {

    private static ProfilesModel instance;
    private static Object selectedItem;

    private ProfilesModel(List<Profile> profiles) {
        for (Profile profile : profiles) {
            addElement(profile);
        }
   
    }

    public static ProfilesModel getInstance() {
        if (instance == null) {
            Profiles pr = (Profiles) HelperJaxb.load(Profiles.class);
            instance = new ProfilesModel(pr.getList());
        }
        return instance;
    }

    public void save() {
        List<Profile> profiles = new ArrayList<>(getSize());
        Enumeration<Profile> elements = elements();
        while (elements.hasMoreElements()) {
            profiles.add(elements.nextElement());
        }

        HelperJaxb.save(new Profiles(profiles));
    }

    @Override
    public void setSelectedItem(Object o) {
        selectedItem = o;
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }
}
