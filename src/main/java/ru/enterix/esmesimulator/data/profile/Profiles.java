/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.data.profile;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;


/**
 *
 * @author SoftProfi
 */
@XmlRootElement
public class Profiles
{

    @XmlElement(required = true)
    private List<Profile> profiles;

    private Profiles ()
    {
        
    }

    public Profiles (
    List<Profile> profiles)
    {
        this.profiles = profiles;
    }
    
    @XmlTransient 
    public void setList(List<Profile> list){
        this.profiles=list;
    }
    
 
    public List<Profile> getList(){
       if(profiles==null){
           profiles= new ArrayList<>();
       }
       return profiles;
    }
            
 }
