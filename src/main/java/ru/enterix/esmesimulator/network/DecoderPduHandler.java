/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.network;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import org.apache.log4j.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import ru.enterix.nettysmpplib.exceptions.ParserPduException;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public class DecoderPduHandler
        extends FrameDecoder {

    private static final Logger LOG = Logger.getLogger(DecoderPduHandler.class);

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer)
            throws Exception {
        if (buffer.readableBytes() < 4) {
            return null;
        }

        buffer.markReaderIndex();
        int length = buffer.readInt();

        if (length < 4) {
            throw new ParserPduException("Invalid lenght pdu. length<4");
        }

        if (buffer.readableBytes() < length - 4) {
            buffer.resetReaderIndex();
            return null;
        }

        ChannelBuffer bytesForPdu = buffer.readBytes(length - 4);

        Pdu pdu = Pdu.createPdu(bytesForPdu.array());

        return pdu;
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        Throwable cause = e.getCause();
        ChannelBuffer bufferError = null;
        if (ParserPduException.class.isAssignableFrom(cause.getClass())) {
            bufferError = ((ParserPduException) cause).getPduBuffer();
        }
        StringBuilder errorLoggingBuffer = new StringBuilder();
        readBytersError(errorLoggingBuffer, bufferError);

        String stringStackTrace = getStringStackTrace(cause);
        String HTMLStackTrace = convertStringStatckTraceToHTML(stringStackTrace);

        errorLoggingBuffer.append("<table cellspacing=\"5\">");
        errorLoggingBuffer.append("<tr>");
        errorLoggingBuffer.append("<td width=\"150\"></td>");
        errorLoggingBuffer.append("<td>");
        errorLoggingBuffer.append(HTMLStackTrace);
        errorLoggingBuffer.append("</td>");
        errorLoggingBuffer.append("</tr>");
        errorLoggingBuffer.append("</table>");
        
        LOG.error(errorLoggingBuffer.toString());
        
        cause.printStackTrace();

    }

    private void readBytersError(StringBuilder errorLoggingBuffer, ChannelBuffer bufferError) {
        if (bufferError == null) {
            return;
        }
        List<Dump> dumps = Dump.convertBytesToDump(bufferError.array(), 16);
        errorLoggingBuffer.append("<table cellspacing=\"5\">");
        for (Dump dump : dumps) {
            errorLoggingBuffer.append("<tr>");
            errorLoggingBuffer.append("<td width=\"100\"></td>");

            errorLoggingBuffer.append("<td width=\"300\">");
            errorLoggingBuffer.append(dump.bytesString);
            errorLoggingBuffer.append("</td>");
            errorLoggingBuffer.append("<td>");
            errorLoggingBuffer.append(dump.chars);
            errorLoggingBuffer.append("</td>");
            errorLoggingBuffer.append("</tr>");
        }
        errorLoggingBuffer.append("</table>");
    }

    private static String convertStringStatckTraceToHTML(String stringStackTrace) {
        String beginStringRegex = "\tat ";
        String endStringRegex = System.getProperty("line.separator");
        String beginStringReplacement = "<p color=\"red\">";
        String endStringReplacement = "</p>";
        String result;
        result = stringStackTrace.replaceAll(beginStringRegex, beginStringReplacement);
        result = result.replaceAll(endStringRegex, endStringReplacement);
        return result;

    }

    private static String getStringStackTrace(Throwable ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

}
