/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.network;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import org.apache.log4j.Logger;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import ru.enterix.esmesimulator.exceptions.NotConnectToServerException;
import ru.enterix.esmesimulator.ui.DisconnectListener;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public class ClientSmppHandler
        extends SimpleChannelHandler {

    private static final Logger LOG = Logger.getLogger(ClientSmppHandler.class);
    private List<DisconnectListener> observerDisconnect = new ArrayList<>();
    private PduLogging pduLogging = new PduLogging(ClientSmppHandler.class);

    private static Channel channel;

    public void connecting(InetSocketAddress serverAddress) throws Exception {

        ChannelFactory factory
                = new NioClientSocketChannelFactory(Executors.newFixedThreadPool(1), Executors.newFixedThreadPool(4));
        final ClientSmppHandler clientSmppHandler = this;
        ClientBootstrap bootstrap = new ClientBootstrap(factory);
        bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            public ChannelPipeline getPipeline() throws Exception {
                return Channels.pipeline(new DecoderPduHandler(), new EncoderPduHandler(), clientSmppHandler);
            }
        });
        ChannelFuture connectFuture = bootstrap.connect(serverAddress);
        channel = connectFuture.awaitUninterruptibly().getChannel();

        if (!connectFuture.isSuccess()) {
            bootstrap.releaseExternalResources();
            throw new NotConnectToServerException("Can not connect to " + serverAddress.toString());
        }

    }

    public void desconecting() {
        channel.close();
        channel.getCloseFuture().awaitUninterruptibly();
    }

    public void writeToChannel(Pdu pdu) {
        pduLogging.doLogTX(pdu);
        channel.write(pdu);
    }

    public void addDisconnectListener(DisconnectListener disc) {
        observerDisconnect.add(disc);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        Pdu pdu = (Pdu) e.getMessage();
        if (pdu == null) {
            return;
        }
        pduLogging.doLogRX(pdu);
    }

    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        LOG.info("<p align=\"left\">The connection is successfully completed. Address=" + e.getChannel().getRemoteAddress() + "/<p>");
    }

    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        LOG.info("<p align=\"left\">Channel Disconnected. Address=" + e.getChannel().getRemoteAddress() + "/<p>");
        for (DisconnectListener disconnectListener : observerDisconnect) {
            disconnectListener.Disconnected();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        System.err.println(e.getCause());
    }

}
