/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.network;

import java.lang.reflect.Field;
import java.util.List;
import org.apache.log4j.Logger;
import ru.enterix.esmesimulator.Classes;
import ru.enterix.esmesimulator.Parser;
import ru.enterix.esmesimulator.data.HelperJaxb;
import ru.enterix.esmesimulator.data.pduError.PduError;
import ru.enterix.esmesimulator.data.pduError.PduErrors;
import static ru.enterix.esmesimulator.network.Dump.convertBytesToDump;
import ru.enterix.nettysmpplib.pdus.Address;
import ru.enterix.nettysmpplib.pdus.AddressRange;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public final class PduLogging {

    private Logger LOG;
    private List<PduError> errors;

    public PduLogging(Class clazz) {
        LOG = Logger.getLogger(clazz);
        PduErrors load = (PduErrors) HelperJaxb.load(PduErrors.class);
        errors = load.getList();
    }

    public void doLogTX(Pdu pdu) {
        StringBuilder logBuffer = new StringBuilder();
        logBuffer.append("<p align=\"left\">");
        logBuffer.append("<b>TX: </b>");

        logBuffer.append("<b>");
        logBuffer.append(pdu.getClass().getSimpleName());
        logBuffer.append("</b>");
        readHead(logBuffer, pdu);
        logBuffer.append("</p>");
        readMainProperties(logBuffer, pdu);
        readError(pdu, logBuffer);

        readDump(pdu.getBytesFromPdu(), logBuffer);

        logBuffer.append("<br>");
        LOG.info(logBuffer.toString());

    }

    public void doLogRX(Pdu pdu) {
        StringBuilder logBuffer = new StringBuilder();
        logBuffer.append("<p align=\"left\">");
        logBuffer.append("<b>RX: </b>");

        logBuffer.append("<b>");
        logBuffer.append(pdu.getClass().getSimpleName());
        logBuffer.append("</b>");
        readHead(logBuffer, pdu);
        logBuffer.append("</p>");
        readMainProperties(logBuffer, pdu);
        readError(pdu, logBuffer);

        readDump(pdu.getBytesFromPdu(), logBuffer);

        logBuffer.append("<br>");
        LOG.info(logBuffer.toString());

    }

    private void readHead(StringBuilder buffer, Pdu pdu) {
        buffer.append("<b>");
        buffer.append(" Head -> ");
        buffer.append(" id= ");
        buffer.append("</b>");
        buffer.append(pdu.getId());
        buffer.append("<b>");
        buffer.append(" status= ");
        buffer.append("</b>");
        buffer.append(pdu.getStatus());
        buffer.append("<b>");
        buffer.append(" number= ");
        buffer.append("</b>");
        buffer.append(pdu.getNumber());
    }

    private void readMainProperties(StringBuilder buffer, Pdu pdu) {
        List<Field> properties = Classes.getMainProperties(pdu.getClass());

        StringBuilder localBuffer = new StringBuilder();
        for (Field field : properties) {
            if (isHead(field)) {
                continue;
            }
            localBuffer.append("<b>");
            localBuffer.append(field.getName());
            localBuffer.append("= ");
            localBuffer.append("</b>");
            localBuffer.append(convertFieldToString(pdu, field));
            localBuffer.append(" ");

        }

        if (localBuffer.length() == 0) {
            return;
        }

        buffer.append("<table cellspacing=\"5\">");
        buffer.append("<tr>");
        buffer.append("<td width=\"100\"></td>");
        buffer.append("<td>");
        buffer.append(localBuffer);
        buffer.append("</td>");
        buffer.append("</tr>");
        buffer.append("</table>");
    }

    private void readError(Pdu pdu, StringBuilder buffer) {
        int status = pdu.getStatus();
        if (status == 0) {
            return;
        }
        PduError findError = findError(errors, status);
        if (findError == null) {
            return;
        }
        buffer.append("<table>");
        buffer.append("<tr>");
        buffer.append("<td width=\"100\"></td>");
        buffer.append("<td>");
        buffer.append("<p color=\"red\">");
        buffer.append(findError.getName());
        buffer.append(": ");
        buffer.append(findError.getDescription());
        buffer.append("</p>");
        buffer.append("</td>");
        buffer.append("</tr>");
        buffer.append("</table>");

    }

    private void readDump(byte bytesFromPdu[], StringBuilder buffer) {
        buffer.append("<table cellspacing=\"5\">");
        for (Dump dump : convertBytesToDump(bytesFromPdu, 16)) {
            buffer.append("<tr>");
            buffer.append("<td width=\"150\"></td>");

            buffer.append("<td width=\"300\">");
            buffer.append(dump.bytesString);
            buffer.append("</td>");
            buffer.append("<td>");
            buffer.append(dump.chars);
            buffer.append("</td>");
            buffer.append("</tr>");
        }
        buffer.append("</table>");
    }

    private static boolean isHead(Field f) {
        boolean result = false;

        if (f.getName().equalsIgnoreCase("id")) {
            result = true;
        } else if (f.getName().equalsIgnoreCase("status")) {
            result = true;
        } else if (f.getName().equalsIgnoreCase("number")) {
            result = true;
        }

        return result;
    }

    private static PduError findError(List<PduError> listError, int value) {
        PduError result = null;
        for (PduError er : listError) {
            int valueInt = Parser.parseInt(er.getValue());
            if (value == valueInt) {
                result = er;
                break;
            }
        }

        return result;
    }

    private static String convertFieldToString(Pdu pdu, Field f) {
        f.setAccessible(true);
        String result = "";

        Class<?> type = f.getType();
        try {
            if (type == byte.class) {
                result = Byte.toString(f.getByte(pdu));
            } else if (type == short.class) {
                result = Short.toString(f.getShort(pdu));
            } else if (type == int.class) {
                result = Integer.toString(f.getInt(pdu));
            } else if (type == String.class) {
                result = (String) f.get(pdu);
            } else if (type == Address.class) {
                Address address = (Address) f.get(pdu);
                result = String.format("%d;%d;%s;", address.getTon(), address.getNpi(), address.getAddress());
            } else if (type == AddressRange.class) {
                AddressRange address = (AddressRange) f.get(pdu);
                result = String.format("%d;%d;%s;", address.getTon(), address.getNpi(), address.getAddress());
            }

        } catch (IllegalArgumentException | IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }

        return result;

    }

}
