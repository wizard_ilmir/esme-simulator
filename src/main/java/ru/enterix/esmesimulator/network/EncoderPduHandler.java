/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.network;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import ru.enterix.nettysmpplib.pdus.Pdu;

/**
 *
 * @author SoftProfi
 */
public class EncoderPduHandler
extends OneToOneEncoder
{

    @Override
    protected Object encode (ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception
    {
        Pdu pdu = (Pdu) msg;
        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(pdu.getBytesFromPdu());
        return buffer;
    }
    
    

}
