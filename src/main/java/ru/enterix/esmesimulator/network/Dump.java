/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator.network;

import java.util.ArrayList;
import java.util.List;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;

/**
 *
 * @author ILMIR
 */
public class Dump {

    public String bytesString;
    public String chars;

    public Dump(String bytesString, String chars) {
        this.bytesString = bytesString;
        this.chars = chars;
    }

    public static List<Dump> convertBytesToDump(byte[] bytes, int sizeLine) {

        List<Dump> result = new ArrayList<>();

        ChannelBuffer buff = ChannelBuffers.wrappedBuffer(bytes);
        ChannelBuffer readBytes;
        while (buff.readable()) {
            if (buff.readableBytes() > sizeLine) {
                readBytes = buff.readBytes(sizeLine);

            } else {
                readBytes = buff.readBytes(buff.readableBytes());
            }
            String bytesString = convertBytesToString(readBytes.array());
            String groupBytesString = groupingString(bytesString, 4);
            String chars = convertBytesToChars(readBytes.array());
            result.add(new Dump(groupBytesString, chars));

        }

        return result;

    }

    private static String groupingString(String source, int count) {
        char[] sourceChars = source.toCharArray();
        StringBuilder result = new StringBuilder();
        int i = 0;
        for (char c : sourceChars) {
            result.append(c);
            i++;
            if (i == count) {
                i = 0;
                result.append("  ");
            }
        }

        return result.toString();

    }

    private static String convertBytesToChars(byte[] bytes) {
        char[] result = new char[bytes.length];
        ChannelBuffer buffer = ChannelBuffers.wrappedBuffer(bytes);

        for (int i = 0; buffer.readable(); i++) {
            result[i] = (char) buffer.readByte();
        }

        return new String(result);

    }

    private static final char[] hexArray
            = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private static String convertBytesToString(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}
