/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import ru.enterix.nettysmpplib.pdus.tlvs.TLV;

/**
 *
 * @author SoftProfi
 */
public final class Classes {

    private static final Logger LOG = Logger.getLogger(Classes.class);

    private Classes() {
    }

    public static List<Field> getProperties(Class<?> type) {
        List<Field> result = new ArrayList<>(30);
        Method[] methods = type.getMethods();
        List<Class<?>> listClazzes = getClassHierarchy(type);

        for (Class<?> clazz : listClazzes) {
            for (Field field : clazz.getDeclaredFields()) {
                String setterName = getSetterName(field.getName());
                if (!field.isSynthetic() && existMethod(methods, setterName)) {
                    result.add(field);
                }
            }
        }

        return result;
    }

    public static List<Field> getMainProperties(Class<?> type) {
        List<Field> allProperties = getProperties(type);
        List<Field> mainProperties = new ArrayList<>(allProperties.size());
        for (Field field : allProperties) {
            if (!TLV.class.isAssignableFrom(field.getType())) {
                mainProperties.add(field);
            }

        }
        return mainProperties;

    }

    public static String getSetterName(String fieldName) {
        return "set" + getWithCapitalLetter(fieldName);
    }

    private static List<Class<?>> getClassHierarchy(Class<?> type) {
        LinkedList<Class<?>> listClazzes = new LinkedList<>();
        listClazzes.addFirst(type);
        Class<?> i = type;
        while (i != null && i != Object.class) {
            listClazzes.addFirst(i.getSuperclass());
            i = i.getSuperclass();
        }

        return listClazzes;

    }

    public static boolean existMethod(Method[] methods, String methodName) {
        if (methods == null || methodName == null) {
            return false;
        }
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return true;
            }
        }
        return false;
    }

    private static String getWithCapitalLetter(String value) {
        return Character.toUpperCase(value.charAt(0)) + value.substring(1);
    }

}
