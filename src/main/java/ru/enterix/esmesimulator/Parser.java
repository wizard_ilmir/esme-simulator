/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.enterix.esmesimulator;

import ru.enterix.esmesimulator.exceptions.ParserInvalidValueException;

/**
 *
 * @author ILMIR
 */
public final class Parser {

    private Parser() {
    }

    public static byte parseByte(String value) throws ParserInvalidValueException {
        try {
            byte result;

            if (value.isEmpty()) {
                result = 0;
            } else if (value.indexOf("x") != -1) {
                result = (byte) Byte.parseByte(value.substring(2), 16);
            } else if (value.indexOf("b") != -1) {
                result = (byte) Byte.parseByte(value.substring(2), 2);
            } else {
                result = Byte.parseByte(value);
            }
            return result;
        } catch (NumberFormatException ex) {
            String descriptionError = String.format("Unable to convert the \"%s\" to Byte", value);
            ParserInvalidValueException newEx = new ParserInvalidValueException(descriptionError);
            newEx.setInvalidValue(value);
            newEx.setParseType(Byte.class);

            throw newEx;
        }
    }

    public static short parseShort(String value) throws ParserInvalidValueException {
        try {
            short result;
            if (value.isEmpty()) {
                result = 0;
            } else if (value.indexOf("x") != -1) {
                result = Short.parseShort(value.substring(2), 16);
            } else if (value.indexOf("b") != -1) {
                result = Short.parseShort(value.substring(2), 2);
            } else {
                result = Short.parseShort(value);
            }
            return result;
        } catch (NumberFormatException ex) {
            String descriptionError = String.format("Unable to convert the \"%s\" to Short", value);
            ParserInvalidValueException newEx = new ParserInvalidValueException(descriptionError);
            newEx.setInvalidValue(value);
            newEx.setParseType(Short.class);

            throw newEx;
        }
    }

    public static int parseInt(String value) throws ParserInvalidValueException {
        try {
            int result;

            if (value.isEmpty()) {
                result = 0;
            } else if (value.indexOf("x") != -1) {
                result = (int) Long.parseLong(value.substring(2), 16);
            } else if (value.indexOf("b") != -1) {
                result = (int) Long.parseLong(value.substring(2), 16);
            } else {
                result = (int) Long.parseLong(value);
            }
            return result;
        } catch (NumberFormatException ex) {
            String descriptionError = String.format("Unable to convert the \"%s\" to Integer", value);
            ParserInvalidValueException newEx = new ParserInvalidValueException(descriptionError);
            newEx.setInvalidValue(value);
            newEx.setParseType(Integer.class);

            throw newEx;
        }
    }

}
